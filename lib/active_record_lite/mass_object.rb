class MassObject



  # takes a list of attributes.
  # creates getters and setters.
  # adds attributes to whitelist.
  def self.my_attr_accessible(*attributes)
    @attributes = []
    attributes.each { |attri| @attributes << attri}
  end

  # returns list of attributes that have been whitelisted.
  def self.attributes
    @attributes
  end

  # takes an array of hashes.
  # returns array of objects.
  def self.parse_all(results)
    results.map do |result|
      self.new(result)
    end
  end

  # takes a hash of { attr_name => attr_val }.
  # checks the whitelist.
  # if the key (attr_name) is in the whitelist, the value (attr_val)
  # is assigned to the instance variable.
  def initialize(params = {})
    #called this way because define_method is an class method otherwise   undefined for instances of objects
    self.class.new_attr_accessor(*params.keys)

    params.each do |variable, value|
      if self.class.attributes.include?(variable.to_sym)
        instance_variable_set("@#{variable}".to_sym, value)
      else
        raise "mass assignment to unregistered attribute #{variable}"
      end
    end

  end

end

#needs to be defined
#could also define this as a class method of object

class Class
  def new_attr_accessor(*args)
    args.each do |arg|
      define_method("#{arg}".to_sym) { instance_variable_get("@#{arg}".to_sym) }
      define_method("#{arg}=".to_sym) { |new_val| instance_variable_set("@#{arg}".to_sym, new_val)}
    end
  end
end